var http = require('http');
var fs = require('fs');
var autoRedirect = "index.html";

http.createServer(function (req, res) {
    var extension = readLineExtension(req.url);
    var dir = __dirname + "/www/";
    var s = false;
    console.log("Request: " + req.url);
    if(req.method == "POST")
    {
        console.log("Authenticating user...");
        res.writeHead(200, {'Content-Type': 'text/html'});
        var mod = require(dir + "auth.nj.js");
        mod.main(req.headers.code)
        
    }
    else{
    //#region Hypertext GET request
    if(extension == "")
    {
        if(fs.existsSync(dir + req.url.substr(1).replace("/", "/").replace(readUrlQueries(req.url), "") + autoRedirect))
        {
            dir += req.url.substr(1).replace("/", "/").replace(readUrlQueries(req.url), "") + autoRedirect;
            s = true;
        }
        else
        {
            res.writeHead(404, {'Content-Type': 'text/html'});
            res.end('Not found!');
        }
    }
    else if (fs.existsSync(dir + req.url.substr(1).replace("/", "/").replace(readUrlQueries(req.url), "")))
    {
        dir += req.url.substr(1).replace("/", "/").replace(readUrlQueries(req.url), "");
        s = true;
    }
    else
    {   
        res.writeHead(404, {'Content-Type': 'text/html'});
        res.end('Not found!');
    }

    if(s == true && fs.existsSync(dir)) 
    {
        if(readLineExtension(dir.replace(".js", "")) == "jn")
        {
            require(dir);
            res.end("hh");
        }
        else
        {
            res.end(fs.readFileSync(dir));
        }  
    }
    else
    {
        console.log(dir);
        res.writeHead(404, {'Content-Type': 'text/html'});
        res.end('Not found!');
    }
    //#endregion
    }
}).listen(8080); 
function readLineExtension(path)
{
    var extension = "";
    if(path.includes("?"))
    {
        for(var i = path.indexOf("?") - 1; i >= 0; i--)
        {
            if(path.charAt(i) == '.') return extension;
            else
            {
                extension += path.charAt(i);
            }
        }
    }
    else
    {  
        for(var i = path.length - 1; i >= 0; i--)
        {
            if(path.charAt(i) == '.') return extension;
            else
            {
                extension += path.charAt(i);
            }
        }
    }
    return "";
}
function readFileName(path)
{
    var extension = "";
    if(path.includes("?"))
    {
        for(var i = path.indexOf("?") - 1; i >= 0; i--)
        {
            if(path.charAt(i) == '/') return extension;
            else
            {
                extension += path.charAt(i);
            }
        }
    }
    else
    {  
        for(var i = path.length - 1; i >= 0; i--)
        {
            if(path.charAt(i) == '/') return extension;
            else
            {
                extension += path.charAt(i);
            }
        }
    }
    return "";
}
function readUrlQueries(path)
{
    var extension = "";
    for(var i = path.length; i >= 0; i--)
    {
        if(path.charAt(i) == '?') return "?" + extension.split("").reverse().join("");
        else
        {
            extension += path.charAt(i);
        }
    }
    return "";
}
