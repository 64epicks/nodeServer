if(getParameterByName("callback", window.location.href) == "1")
{
    var http = new XMLHttpRequest();
    var url = 'auth.nj.js';
    var params = '';
    http.open('POST', url, true);

    //Send the proper header information along with the request
    http.setRequestHeader('code', getParameterByName("code", window.location.href));

    http.onreadystatechange = function() {//Call a function when the state changes.
    if(http.readyState == 4 && http.status == 200) {
        alert(http.responseText);
    }
}
http.send(params);
}
function addQuery(key, value)
{
    var startchar = "";
    if(window.location.href.split('?').length == 1) startchar = "?";
    else startchar = "&";
    history.pushState({
        id: 'home'
    }, 'Hem | Exempel', window.location.href + startchar + key + "=" + value);
}
function removeQuery(key)
{
    var url = window.location.href;
    var urlparts = url.split('?');   
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(key)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {    
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                pars.splice(i, 1);
            }
        }

        url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
        history.pushState({
            id: 'home'
          }, 'Hem | Exempel', url);
    }
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}