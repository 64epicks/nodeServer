


updatePage();

function setPage(page){
  history.pushState({
    id: 'about'
  }, 'Exempel', window.location.href.split('?')[0] + '?tab=' + page);
  updatePage();
}
function updatePage(){
var tab = getParameterByName('tab', window.location.href);
if(tab == 'news'){
  document.getElementById("page").innerHTML = pageGet("/pages/news/");
  removeQuery("tab");
  addQuery("tab", "news");

  document.getElementById("nyheter").style.backgroundColor = "#009EFF";
  document.getElementById("kontakt").style.backgroundColor = "#333";
  document.getElementById("om").style.backgroundColor = "#333";
  document.getElementById("hem").style.backgroundColor = "#333";
  
}
else if (tab == 'contact'){
  document.getElementById("page").innerHTML = pageGet("/pages/contact/");
  removeQuery("tab");
  addQuery("tab", "contact");

  document.getElementById("kontakt").style.backgroundColor = "#009EFF";
  document.getElementById("nyheter").style.backgroundColor = "#333";
  document.getElementById("om").style.backgroundColor = "#333";
  document.getElementById("hem").style.backgroundColor = "#333";
}
else if (tab == 'login'){
  document.getElementById("page").innerHTML = pageGet("/pages/login/");
  removeQuery("tab");
  addQuery("tab", "login");

  document.getElementById("om").style.backgroundColor = "#009EFF";
  document.getElementById("kontakt").style.backgroundColor = "#333";
  document.getElementById("nyheter").style.backgroundColor = "#333";
  document.getElementById("hem").style.backgroundColor = "#333";
}
else{
  document.getElementById("page").innerHTML = pageGet("/pages/home/");
  removeQuery("tab");
  addQuery("tab", "home");

  document.getElementById("hem").style.backgroundColor = "#009EFF";
  document.getElementById("om").style.backgroundColor = "#333";
  document.getElementById("kontakt").style.backgroundColor = "#333";
  document.getElementById("nyheter").style.backgroundColor = "#333";
}
}
function addQuery(key, value)
{
    var startchar = "";
    if(window.location.href.split('?').length == 1) startchar = "?";
    else startchar = "&";
    history.pushState({
        id: 'home'
    }, 'Hem | Exempel', window.location.href + startchar + key + "=" + value);
}
function removeQuery(key)
{
    var url = window.location.href;
    var urlparts = url.split('?');   
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(key)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {    
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                pars.splice(i, 1);
            }
        }

        url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
        history.pushState({
            id: 'home'
          }, 'Hem | Exempel', url);
    }
}
function pageGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}